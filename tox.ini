[tox]
minversion = 3.8.6
envlist =
    lint
    sanity
    unittest
skipdist = True
skip_missing_interpreters = True

[travis]
python =
  3.7: lint, sanity

[flake8]
max-line-length = 90
max-complexity = 10

[testenv]
usedevelop = True
basepython = python3.7
passenv = *
setenv =
    PYTHONDONTWRITEBYTECODE=1
commands_pre =
    find {toxinidir} -type f -not -path '{toxinidir}/.tox/*' -path \
        '*/__pycache__/*' -path '*/build/*' -name '*.py[c|o]' -delete
whitelist_externals =
    find
    bash
    wget
    unzip
    rm
# Enabling sitepackages is needed in order to avoid encountering exceptions
# caused by missing selinux python bindinds in ansible modules like template.
# Selinux python bindings are binary and they cannot be installed using pip
# in virtualenvs. Details: https://github.com/ansible/molecule/issues/1724
sitepackages = false

[testenv:lint]
deps =
    flake8>=3.6.0,<4
    yamllint>=1.11.1,<2
skip_install = true
usedevelop = false
commands =
    flake8 --exclude=.tox,template,.eggs,.pytest_cache
    bash -ec "yamllint $(find . -path ./template -prune -o -path ./tests -prune \
        -o -name '*.yml' -print)"

[testenv:sanity]
deps =
    pluggy==0.12.0
    pytest==4.5.0
    PyYAML==5.1
skip_install = true
usedevelop = false
commands_pre =
    bash {toxinidir}/tools/scripts/update-template.sh https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/-/archive/master/piperci-faas-templates-master.tar.gz?path=template {envdir}
    bash {toxinidir}/tools/scripts/install-mindflayer.sh {envdir} {toxworkdir}/.tmp
commands =
    pytest {envdir}/tests/test_stack_yaml.py

[testenv:unittest]
deps =
  codecov
  pytest-flask
  pytest-cov
  pytest-mock
  responses
  pytest-server-fixtures[s3]
  pyyaml
passenv = CI CODECOV_TOKEN
usedevelop = true
whitelist_externals =
    find
    bash
    pkill
commands_pre =
  bash {toxinidir}/tools/scripts/install-minio.sh {envbindir}
  bash {toxinidir}/tools/scripts/update-template.sh https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates/-/archive/master/piperci-faas-templates-master.tar.gz?path=template {envdir}

  bash {toxinidir}/tools/scripts/setup-unit-tests.sh {envdir}/build {envdir}/template

  find {toxinidir} -type f -not -path '{toxinidir}/.tox/*' -path '*/__pycache__/*' -path '*/build/*' -name '*.py[c|o]' -delete
  bash -c "pkill minio || true"
  bash -c 'reqfiles=''; for reqfile in {envdir}/build/*/requirements.txt; do reqfiles="$reqfiles -r $reqfile"; done; pip install $reqfiles'
commands_post =
  bash -c "pkill minio || true"
commands =
    bash -c 'MODULE_ROOT={envdir}/build pytest tests \
     --cov={envdir}/build/piperci_noop_gateway/function \
     --cov={envdir}/build/piperci_noop_executor/function \
     --cov-report=term-missing \
     --no-cov-on-fail'
    codecov
