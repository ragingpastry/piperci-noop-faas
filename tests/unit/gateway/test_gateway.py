import responses

from piperci.storeman.client import storage_client


def test_gateway_no_json(client):
    headers = {"Content-Type": "application/json"}
    resp = client.post("test", headers=headers)
    assert resp.status_code == 422


def test_gateway_no_content_type_json(client, single_instance):
    resp = client.post("test", data=single_instance)
    assert resp.status_code == 422


@responses.activate
def test_gateway(
    client,
    common_headers,
    minio_server,
    mocker,
    single_instance,
    start_task_response,
    task_artifact_upload_response,
    task_creation_response,
    task_post_executor_url_response,
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(*task_post_executor_url_response[0])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    mocker.patch("piperci.faas.this_task.ThisTask._init_storage_client", store_cli)

    resp = client.post("test", json=single_instance, headers=common_headers)
    assert resp.status_code == 200
